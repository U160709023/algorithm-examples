
public class MyStack<E extends Object> {
	private SinglyLinkedList<E> list;

	public MyStack() {
	list = new SinglyLinkedList<E>();
	}

	public void push(E x) {
		
		list.insert(this.list,x);
	}
	
	public Object peek() {
		return list.getLastElement();
	}
	
	public int size() {
		return list.getSize();
	}
	
	public E pop() {
		E x =(E) list.getLastElement();
		list.DeleteElementAtIndex(list, list.getSize()-1);

		return x;
	}
	public boolean isEmpty() {
		return list.getSize() == 0;
	}
	
	public void seeStack() {
		list.printList(list);
	}
}
