
public class GenericClass<T> {
	private T t;
	
	private T get() {
		return this.t;
	}
	
	public void set(T t) {
		this.t = t;
	}
	
	
	
	public static void main(String args[]){
		
		GenericClass<Integer> myGC = new GenericClass<Integer>();
		
		myGC.t = 5;
		
		System.out.println(myGC.get());
		
		myGC.set(6);
		System.out.println(myGC.get());
			
		
		GenericClass Ali = new GenericClass();
		

		System.out.println(Ali.get());
		
		Ali.set(8);
		System.out.println(Ali.get());
		
		Ali = myGC;
		System.out.println(Ali.get());

		System.out.println(Ali + "" + myGC);
		
		GenericClass<Integer> Veli = Ali;
		System.out.println(Ali + "" + Veli);
		System.out.println(System.identityHashCode(Ali)+ "//" + System.identityHashCode(Veli));

	
	}
}
