
public class BinaryTree<E> {
	Node firstNode;
	
	public void insert(BinaryTree<E> Tree, E elem) {
		
		Node new_Node = new Node<E>(elem);
		
		if(firstNode == null) {
			firstNode = new_Node;		}
		else {
			Node traversal = Tree.firstNode;
			while(traversal.RightChild != null) {
				traversal = traversal.RightChild;

				
			}
		
				if(traversal.leftChild == null) {
					traversal.leftChild = new_Node;
					traversal.leftChild.Parent = traversal;
					
				}
				
				else {
					traversal.RightChild = new_Node;
					traversal.RightChild.Parent = traversal;
					

				}
			
				
			
			
		}
		
		
		
	}
		
	
		public void DeleteElement(BinaryTree<E> Tree, E elem) {
			Node traversal = Tree.firstNode;
			while(traversal != null) {
				if(traversal.Data == elem) {
					if(traversal.leftChild != null) {
						insert(Tree, (E) traversal.leftChild.Data);
						}
					traversal.Parent.RightChild = traversal.RightChild;
					
				}
				traversal = traversal.RightChild;
			}
			
		}
		public void printTree(BinaryTree<E> Tree) {
			boolean x = true;
			
			Node traversal = Tree.firstNode;
			while(traversal.leftChild != null ) {
				x = false;
				System.out.println("left child of parent" + traversal.Data + "is" + traversal.leftChild.Data);
				if(traversal.RightChild != null) {
					System.out.println("right child of parent" + traversal.Data + "is" + traversal.RightChild.Data);
					traversal = traversal.RightChild;

				}
				else {
					break;
				}
			}
			if (x) {
				System.out.println(traversal.Data);
			}
			
		}
		class Node<T>{
		Node leftChild;
		Node RightChild;
		Node Parent;
		T Data;
		public Node(T D) {
			this.Data = D;
			leftChild = null;
			RightChild = null;
			
		}
		
		
	}
}
