import java.text.DecimalFormat;
import java.time.format.DecimalStyle;
import java.util.LinkedList;

import javax.xml.ws.soap.Addressing;

public class SinglyLinkedList<T> {


	 class Node<E> {
		E Data;
		Node next;
		
		public Node(E D) {//Every node should contain A data
			this.Data = D;
			//next = null;
			
		}
	 
	 }
		Node head;

		
//		public static void main(String[] args) 
//	    { 
//	        /* Start with the empty list. */
//	        SinglyLinkedList list = new SinglyLinkedList(); 
//	  
//	        // 
//	        // ******INSERTION****** 
//	        // 
//	  
//	        // Insert the values 
//	        list.insert(list, 1); 
//	        list.insert(list, 2); 
//	        list.insert(list, 3); 
//	        list.insert(list, 4); 
//	        list.insert(list, 5); 
//	        list.insert(list, 6); 
//	        list.insert(list, 7); 
//	        list.insert(list, 8); 
//	        // Print the LinkedList 
//	        list.printList(list); 
//	        list.getData(list, 20);
//	        list.DeleteData(list, 1);
//	        //list.DeleteData(list, 5);
//	     
//	        list.DeleteData(list, 8);
//	        list.DeleteData(list, 6);
//
//	        list.printList(list); 
//	        //list.DeleteData(list, 1);
//	        //list.printList(list); 
//
//
//	    } 
//		
		
	

	
	public  SinglyLinkedList insert(SinglyLinkedList list, T data) {//adding new element to out linkedlist
		
		Node new_Node = new Node(data);//Firstly, create new node to add in an element
		
		new_Node.next = null;//next element has to be null 
		if(list.head == null) {//if our list's head is null then "new_node equal to head of the list
			list.head = new_Node;
		}
		else {
		Node lastElement = list.head;
		//System.out.println("DENEME 123"+ lastElement + list.head.next);
		while(lastElement.next != null) {
			lastElement = lastElement.next;
			//1252169911 ---- 685325104
		}
		lastElement.next = new_Node;
	//	System.out.println("data: " + data + "eklendi");

		}
	
		return list;
	}
	
	public  void printList(SinglyLinkedList list) {
		

        Node currNode = list.head; 
   
        System.out.print("LinkedList: "); 
   
        // Traverse through the LinkedList 
        while (currNode != null) { 
            // Print the data at current node 
            System.out.print(currNode.Data + " "); 

   
            // Go to next node 
            currNode = currNode.next; 
        } 
        System.out.println( " "); 

	}
	
	public T getData(SinglyLinkedList list, T data) {
		
		Node  startingPoint = list.head;
		boolean isUlastin = false;
		
		while(startingPoint.next != null) {
			//System.out.println(startingPoint.Data);
			startingPoint = startingPoint.next;
			if(startingPoint.Data == data) {
			//	System.out.println("ulaştın: " + data);
				isUlastin = true;
			}
		}
		if(!isUlastin) {
		//	System.out.println("böyle bir eleman yok");
		}
		return data;
	}
	
	
	public void DeleteData(SinglyLinkedList list, T WillbeDeleted) {
		Node head = list.head;
		if(WillbeDeleted == head.Data) {
			list.head = list.head.next;
			
		}
		else {//ilk elemanın dışındaki elemanlar için
			Node prev = head;
			while(prev.next.next != null) {
				if(prev.next.Data == WillbeDeleted) {
					prev.next = prev.next.next;
				}
				else {
					prev = prev.next;
				}
			}
			
//			if(prev.next.next == null) {//we try to delete last element
//				prev.next = null;
//			}
			Node prev1 = head;
			Node Current = head.next;
			
			while(Current != null) {
			if(Current.next ==null && Current.Data == WillbeDeleted) {
				System.out.println("true");
			
				prev1.next = null;
				break;
			}
			prev1 = prev1.next;
			Current = prev1.next;
			}
		}
		
	}
	public int getSize() {
		Node Element = head;
		int size = 0;
		while(Element != null) {
			size++;
			Element = Element.next;
		}
		return size;
	}
	
	
	
	
	public void DeleteElementAtIndex(SinglyLinkedList<T> list, int Index) {
		try {
			if(Index >= list.getSize()) {
			
				throw new Exception("bunu yapamazsın");
			}
		}catch (Exception e) {
			System.out.println(Index +"max elmandan fazla");
		}
		Node head = list.head;
		if(Index ==0 ) {
			list.head = list.head.next;
			
		}
		
		else {
			int countIndex = 1;
			Node prev = list.head;//CountIndex 0 is prev elem
			while(countIndex != Index && countIndex < list.getSize()) {
				countIndex++;
				prev = prev.next;
				
			}
			
			if(prev.next == null) {
				prev = null;
			}
			else {
				prev.next = prev.next.next;
			}
			
			
		}
		
		
		
	}
	
	public Object getLastElement() {
		Node Element = head;
		
		while(Element != null) {
			Node temp = Element;
			Element = Element.next;
			if(Element == null) {
				Element = temp;
				break;
			}
			
		}
		
		return (Object) Element.Data;
	}
	
	
	}
	


